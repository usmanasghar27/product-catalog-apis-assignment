<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function store(ProductCreateRequest $request): JsonResponse
    {
        $data = $request->all();
        $product = Product::create($data);
        return response()->json(['product' => new ProductResource($product), 'message' => 'Product created successfully']);
    }

    public function products(Request $request)
    {
        $query = Product::query();
        if ($request->has('search') && $request->search != '') {
            $query = $query->where('product_name', 'LIKE', "%$request->search%");
        }
        $products = $query->paginate(10);
        return ProductResource::collection($products);
    }

    public function updateStatus(Request $request, Product $product)
    {
        $validated = $request->validate([
            'is_active' => 'required',
        ]);
        if ($validated) {
            $product->update(['is_active' => $request->is_active]);
        }
        return response()->json(['product' => new ProductResource($product), 'message' => 'Product status updated successfully']);
    }
}
