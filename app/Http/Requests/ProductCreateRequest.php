<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|string|max:128',
            'price' => 'required',
            'cost' => 'required',
            'discount_percentage' => 'numeric|max:100|required_if:is_discounted,1',
            'images' => 'required',
            'unit' => 'required|max:64',
            'weight_per_unit' => 'required'
        ];
    }
}
