<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        if ($data['is_discounted']) {
            $data['discounted_price'] = $data['price'] - ($data['price'] * $data['discount_percentage'] / 100);
        }
        $data['image'] = $this->image;
        return $data;
    }
}
