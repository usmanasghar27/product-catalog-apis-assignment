<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $guarded = [];

    protected $casts = [
        'images' => 'json',
        'is_active' => 'boolean',
        'is_discounted' => 'boolean'
    ];

    public function getImageAttribute()
    {
        return $this->images[0];
    }
}
