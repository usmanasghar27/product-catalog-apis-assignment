<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name', 128);
            $table->double('price');
            $table->double('cost');
            $table->boolean('is_discounted')->default(false);
            $table->float('discount_percentage')->default(0);
            $table->mediumText('description')->nullable();
            $table->json('images');
            $table->boolean('is_active')->default(true);
            $table->string('unit', 64);
            $table->double('weight_per_unit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
